package com.itau.exemplospring.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@RequestMapping("")
	public String hello() {
		return "Hello World";
	}
	
	@RequestMapping(method=RequestMethod.POST, path="")
	public String receber() {
		return "Recebi um POST";
	}
	
	@RequestMapping("/teste")
	public String teste() {
		return "Esse endpoint é um teste";
	}
	
}
