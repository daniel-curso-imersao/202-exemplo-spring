package com.itau.exemplospring.controllers;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.exemplospring.model.Comida;

@RestController
public class CardapioController {
	
	private ArrayList<Comida> getListaDoces()  {
		ArrayList<Comida> doces = new ArrayList<>();
		
		Comida doce1 = new Comida();
		doce1.nome = "Chocolate";
		doce1.tipo = "Doce";
		doce1.calorias = 2000;
		
		Comida doce2 = new Comida();
		doce2.nome = "Pudim";
		doce2.tipo = "Doce";
		doce2.calorias = 10000;
		
		doces.add(doce1);
		doces.add(doce2);
		
		return doces;
	}

	@RequestMapping("/cardapio/doces")
	public ArrayList<Comida> getDoces() {
		return getListaDoces();
	}

	@RequestMapping("/cardapio/doce/{id}")
	public ResponseEntity<Comida> getDoce(@PathVariable int id) {
		ArrayList<Comida> doces = getListaDoces();
		
		if (id > doces.size() - 1) {
			return ResponseEntity.status(404).build();
		}
		
		Comida doce = doces.get(id);
		
		return ResponseEntity.ok(doce);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/cardapio/inserirdoce")
	public ResponseEntity<Comida> inserirDoce(@RequestBody Comida comida) {
		// salvar objeto

		return ResponseEntity.status(201).body(comida);
	}
}
